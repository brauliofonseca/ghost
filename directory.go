package main

import (
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

type Directory struct {
	name  string
	files []*File
}

/* Creates Directory struct that mirrors the current target directory structure
 */
func NewDirectory(rootDir string) *Directory {
	dirFiles := []*File{}
	filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatalf("Error traversing file %s in directory %s\n%s", path, rootDir, err)
		} else {
			if !info.IsDir() {
				filename, err := filepath.Abs(path)
				if err != nil {
					log.Fatalf("Could not find absolute %s\n%s", info.Name(), err)
				} else {
					dirFiles = append(dirFiles, NewFile(filename))
				}
			}
		}
		return nil
	})
	return &Directory{name: rootDir, files: dirFiles}
}

/* Decrypts files in directory:
- Creates a channel for File struct pointers
- Iterates through File* in Directory struct and decrypts using File function as a goroutine
- Wait for all goroutines to finish and close File* channel
- Create new Directory struct with all decrypted files
*/
func (dir *Directory) DecryptDirectoryContent(waitGroup *sync.WaitGroup, password string) *Directory {
	fileChannel := make(chan *File, len(dir.files))
	for _, file := range dir.files {
		waitGroup.Add(1)
		go file.DecryptedFileToChannel(fileChannel, waitGroup, password)
	}
	waitGroup.Wait()
	close(fileChannel)
	decryptedFiles := []*File{}
	for file := range fileChannel {
		log.Printf("Decrypted file: %s", file.name)
		decryptedFiles = append(decryptedFiles, file)
	}
	return &Directory{name: dir.name, files: decryptedFiles}
}

/* Encrypts files in directory:
- Creates a channel for File struct pointers
- Iterates through File* in Directory struct and encrypts using File function as a goroutine
- Wait for all goroutines to finish and close File* channel
- Create new Directory struct with all encrypted files
*/
func (dir *Directory) EncryptDirectoryContent(waitGroup *sync.WaitGroup, password string) *Directory {
	fileChannel := make(chan *File, len(dir.files))
	for _, file := range dir.files {
		waitGroup.Add(1)
		go file.EncryptedFileToChannel(fileChannel, waitGroup, password)
	}
	waitGroup.Wait()
	close(fileChannel)
	encryptedFiles := []*File{}
	for file := range fileChannel {
		log.Printf("Created encrypted file: %s", file.name)
		encryptedFiles = append(encryptedFiles, file)
	}
	for i := 0; i < len(encryptedFiles); i++ {
	}
	return &Directory{name: dir.name, files: encryptedFiles}
}

/* Create new directory in system
- Iterate through files in Directory struct and extract file name
- Create new path which is passed as command line parameter
- Creates new directory if it does not exist
- Writes new files into that new directory
*/
func (dir *Directory) CreateDirectory(newDirName string) {
	var currentPathSlice []string
	for _, file := range dir.files {
		currentPathSlice = strings.Split(file.name, string(os.PathSeparator))
		absolutePath, _ := filepath.Abs(newDirName)
		newFilePath := filepath.Join(absolutePath, currentPathSlice[len(currentPathSlice)-1])
		file.CreateFileInPath(newFilePath)
	}
}
