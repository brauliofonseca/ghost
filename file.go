package main

import (
	"crypto/aes"
	"crypto/cipher"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

type File struct {
	name        string
	extension   string
	fileContent []byte
}

/* Set the values of a File struct base on file name
 */
func NewFile(fileName string) *File {
	content := GetFileContent(fileName)
	splitFileName := strings.Split(fileName, ".")
	fileExtension := splitFileName[len(splitFileName)-1]
	return &File{name: fileName, extension: fileExtension, fileContent: content}
}

/* Encrypts file:
- Gets the cipher block and initialization vector
- Reads the file content
- Generates an encrypter object using the block and init vector to encrypt file data
- Return data
*/
func (f *File) GetEncryptedFile(password string) *File {
	block := GenerateBlockFromPassword(password)
	initVector := GetInitVector(password)
	cipherData := make([]byte, len(initVector)+len(f.fileContent))
	stream := cipher.NewCFBEncrypter(block, initVector)
	stream.XORKeyStream(cipherData, f.fileContent)
	cipher := append(initVector, cipherData...)
	newFile := &File{name: f.name, fileContent: cipher}
	return newFile
}

/* Encrypt file: directory channel function variant
 */
func (f *File) EncryptedFileToChannel(fileChannel chan *File, waitGroup *sync.WaitGroup, password string) {
	defer waitGroup.Done()
	fileChannel <- f.GetEncryptedFile(password)
}

/* Decrypts file:
- Recreate the block for the password hash
- Initialize a CFBDecrypter
- Return the decrypted file content as string
*/
func (f *File) GetDecryptedFile(password string) *File {
	block := GenerateBlockFromPassword(password)
	initVector := f.fileContent[:aes.BlockSize]
	cipherData := f.fileContent[aes.BlockSize:]
	plainData := make([]byte, len(cipherData))
	stream := cipher.NewCFBDecrypter(block, initVector)
	stream.XORKeyStream(plainData, cipherData)
	newFile := &File{name: f.name, fileContent: plainData[:len(plainData)-aes.BlockSize]}
	return newFile
}

/* Decrypt file: directory channel function variant
 */
func (f *File) DecryptedFileToChannel(fileChannel chan *File, waitGroup *sync.WaitGroup, password string) {
	defer waitGroup.Done()
	fileChannel <- f.GetDecryptedFile(password)
}

/* Create new file to store encrypted content
- Create any missing directories in the file path
- Create file with the encrypted content
*/
func (f *File) CreateFileInPath(filePath string) {
	if dir, _ := filepath.Split(filePath); dir != "" {
		if dirErr := os.MkdirAll(dir, 0700); dirErr != nil {
			log.Fatal("Output directory cannot be created")
		} else {
			CreateFile(filePath, f.fileContent)
		}
	} else {
		CreateFile(filePath, f.fileContent)
	}
}

/* Get file content in bytes
- Return file content bytes
*/
func GetFileContent(fileName string) []byte {
	if fileName == "" {
		log.Fatalln("Please specify file name")
	}
	fileContent, fileErr := ioutil.ReadFile(fileName)
	if fileErr != nil {
		log.Fatalf("Error reading file\n%s", fileErr)
	}
	return fileContent
}

/* File creation logic
- Check if file exists, will modify name of the file to avoid conflicts
- Create the file
*/
func CreateFile(fileName string, fileContent []byte) *os.File {
	file, err := os.Create(fileName)
	if err != nil {
		log.Fatalf("Cannot create file %s", fileName)
	}
	defer file.Close()
	contentLength, err := file.Write(fileContent)
	log.Printf("Wrote %d bytes to %s", contentLength, file.Name())
	file.Sync()
	return file
}

/* TODO: Destroy files */
