package main

import (
	"log"
	"math"
	"strings"
	"crypto/aes"
	"crypto/cipher"
	"golang.org/x/crypto/bcrypt"
)

const specialCharacters = "!@#$%^&*()_+-=~`[]{}|\\;:'\"<>?,./"

/* Gets block for AES encryption
	- Using the given password, get block for AES encryption
*/
func GenerateBlockFromPassword(password string) (cipher.Block) {
	paddedPassword := GetPaddedPassword(password)
	block, err := aes.NewCipher(paddedPassword)
	if err != nil {
		log.Fatalln(err)
	}
	return block
}

/* Gets password in bytes
	- Verify the size of the password
	- Will return a 32 byte slice containing password and padded with 
	  repeating password sequence until 32 bytes reached
*/
func GetPaddedPassword(password string) ([]byte) {
	var paddedPassword []byte
	if isValidPassword(password) {
		if len(password) != 32 {
			passwordBytes := []byte(password)
			paddedPassword = make([]byte, len(password))
			copy(paddedPassword, passwordBytes)
			currentPasswordBytes := len(passwordBytes)
			for index := 0; currentPasswordBytes < 32; index++ {
				currentByteIndex := int(math.Abs(float64((len(password) - index) % len(password))))
				paddedPassword = append(paddedPassword, passwordBytes[currentByteIndex])
				currentPasswordBytes += 1
			}
		}
	}
	return paddedPassword
}

/* Get initialization vector
	- Generate init vector bytes from the key using bcrypt
	- Return initialization vector that equals the size of the block
*/
func GetInitVector(password string) []byte {
	var initVector[]byte
	paddedPassword := GetPaddedPassword(password)
	hash, err := bcrypt.GenerateFromPassword(paddedPassword, 10)
	initVector = make([]byte, len(hash))
	copy(initVector, hash)
	if err != nil {
		log.Fatalln("Could not create hash from password")
	}
	return initVector[0:aes.BlockSize]
}

/* Validate if password is length is valid 
	- Validate minimum password length
	- Validate if presence of special characters in password
*/
func isValidPassword(password string) bool {
	if len(password) < 8 {
		log.Fatalln("Password length must be at 8 characters long")
	}
	if !strings.ContainsAny(password, specialCharacters) {
		log.Fatalln("Password must contain at least one special character")
	}
	return true
}

