# Ghost

A simple file encryption command line tool.
This command line tool uses AES/Rijndael encryption standard to obfuscate file content. It uses a 256 bit key to perform the encryption. This is a really interesting encryption algorithm that is worth checking out!

[Amusing AES explanation](http://www.moserware.com/2009/09/stick-figure-guide-to-advanced.html) 


## Usage 

Build the executable with the following command:
```
go build
```

```
Options				Description
---------------------------------------------------
  (Required if decrypting files in a dir)
  -dir
  -directory
        If file specified to encrypt/decrypt is a directory, specify this flag

  (Required to specify either enc or dec)
  -dec
  -decrypt
        Decrypt content of file
  -enc
  -encrypt
        Encrypt content of file

  (Required)
  -f string
  -file string
        File you want to encrypt/decrypt

  (Optional, will output to console if not specified)
  -o string
  -output string
        Path to store encrypted/decrypted file, will output to console if not specified

  (Required)
  -p string
  -password string
        Password used to encrypt/decrypt files
```

## Examples
Encryption 
```
./ghost -encrypt -file testfiles/dummy-testfile.txt -password "GhOsT102@+" -output ./encrypted_dummy.txt

./ghost -enc -dir -f testfiles -p "GhOsT102@+" -o encryptedFiles
```

Decryption
```
./ghost -decrypt -file ./encrypted_dummy.txt -password "GhOsT102@+" -output ./decrypted_dummy.txt

./ghost -dec -dir -f encryptedFiles -p "GhOsT102@+" -o decryptedFiles
```

## Contribute

Please don't hesitate to contribute to this project. This is my first attempt in using golang and I am always learning. Fork this project and submit pull requests!
 
