package main

import (
	"bytes"
	"testing"
)

func TestGetEncryptedFileFunctionToEncryptImageFile(t *testing.T) {
	testFile := NewFile("./testfiles/lena.bmp")
	encryptedFile := testFile.GetEncryptedFile("thisIsABadPAssword$#")
	testFileContent := testFile.fileContent
	encryptedFileContent := encryptedFile.fileContent

	isEqual := bytes.Equal(testFileContent, encryptedFileContent)

	if isEqual {
		t.Errorf("Test file and encrypted file have the same exact bytes, no encryption performed")
	}
}

func TestGetDecryptedFileFunctionToDecryptedChineseTextFile(t *testing.T) {
	originalFile := NewFile("./testfiles/chinese-testfile.txt")
	encryptedFile := NewFile("./testfiles/encrypted-chinese-testfile.txt")
	decryptedFileContent := encryptedFile.GetDecryptedFile("badPassword!@").fileContent

	isEqual := bytes.Equal(decryptedFileContent, originalFile.fileContent)

	if !isEqual {
		t.Errorf("Decrypted test file and original file do not match")
	}
}

func TestCreateFile(t *testing.T) {
}

func TestDeleteFile(t *testing.T) {
}
