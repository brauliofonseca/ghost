package main

import (
	"flag"
	"fmt"
	"log"
	"sync"
)

var output = flag.String("output", "", "Path to store encrypted/decrypted file (will create new file if does not exist), will output to console if not specified")
var password = flag.String("password", "", "Password used to encrypt/decrypt files")
var fileName = flag.String("file", "", "Path of file you want to encrypt/decrypt")
var encrypt = flag.Bool("encrypt", false, "Specify this flag to encrypt file(s)")
var decrypt = flag.Bool("decrypt", false, "Specify this flag to decrypt file(s)")
var isDir = flag.Bool("directory", false, "If file specified to encrypt/decrypt is a directory, specify this flag")

func init() {
	flag.StringVar(output, "o", "", "")
	flag.StringVar(password, "p", "", "")
	flag.StringVar(fileName, "f", "", "")
	flag.BoolVar(encrypt, "enc", false, "")
	flag.BoolVar(decrypt, "dec", false, "")
	flag.BoolVar(isDir, "dir", false, "")
}

func main() {
	flag.Parse()
	if *isDir {
		if *encrypt == true {
			dirEncryptWaitGroup := &sync.WaitGroup{}
			dir := NewDirectory(*fileName)
			encDir := dir.EncryptDirectoryContent(dirEncryptWaitGroup, *password)
			encDir.CreateDirectory(*output)
		} else if *decrypt == true {
			dirDecryptWaitGroup := &sync.WaitGroup{}
			dir := NewDirectory(*fileName)
			decDir := dir.DecryptDirectoryContent(dirDecryptWaitGroup, *password)
			decDir.CreateDirectory(*output)
		} else {
			log.Fatalln("Please specify operation, encrypt or decrypt")
		}
	} else {
		file := NewFile(*fileName)
		var newFile *File
		if *encrypt == true {
			newFile = file.GetEncryptedFile(*password)
		} else if *decrypt == true {
			newFile = file.GetDecryptedFile(*password)
		} else {
			log.Fatalln("Please specify operation, encrypt or decrypt")
		}

		if *output != "" {
			newFile.CreateFileInPath(*output)
		} else {
			fmt.Println("No output file specified, will save to output.txt ...")
			newFile.CreateFileInPath("output.txt")
		}
	}
}
